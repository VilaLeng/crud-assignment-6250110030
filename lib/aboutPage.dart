import 'package:crudmysql_api/theProductList.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:crudmysql_api/navigationDrawer.dart';
import 'package:crudmysql_api/createDrawerBodyItem.dart';
import 'package:crudmysql_api/createDrawerHeader.dart';
import 'dart:io';
import 'main.dart';
import 'aboutPage.dart';

class aboutPage extends StatelessWidget {
  static const String routeName = '/aboutPage';

  @override
  Widget build(BuildContext context) {
    var pageRoutes;
    return new Scaffold(
      appBar: AppBar(
        title: Text("\$ About Developer Page \$",
          style: TextStyle(color: Colors.white54,
              fontWeight: FontWeight.bold,fontSize: 25),),
      ),
      //drawer: navigationDrawer(),
      drawer: Drawer(
        backgroundColor: Colors.blueGrey[200],
        child: ListView(
          padding: EdgeInsets.zero,
          children: <Widget>[
            createDrawerHeader(),
            createDrawerBodyItem(
              icon: Icons.person,
              text: 'About Developer Page',
              onTap: () =>
                  // Navigator.pushReplacementNamed(context, pageRoutes.home),
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => aboutPage())),
            ),
            // createDrawerHeader(),
            createDrawerBodyItem(
              icon: Icons.auto_stories_rounded,
              text: 'The Products List Page',
              onTap: () =>
              // Navigator.pushReplacementNamed(context, pageRoutes.home),
              Navigator.push(context,
                  MaterialPageRoute(
                      builder: (context) => MyApp())),
            ),
            createDrawerBodyItem(
              icon: Icons.book,
              text: 'The Products News Page',
              onTap: () =>
              // Navigator.pushReplacementNamed(context, pageRoutes.home),
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => listPage())),
            ),
          ],
        ),
      ),
      //  body: Center(child: Text("This is profile page"))
      body: Container(
        padding: EdgeInsets.only(
          top: 20,
          left: 20,
          right: 20,
          bottom: 20,
        ),
        color: Colors.grey[200],
        constraints: BoxConstraints.expand(),
        child: SingleChildScrollView(
          child: Column(
            children: [
              Row(
                children: [
                  CircleAvatar(
                    radius: 41,
                    backgroundImage: AssetImage('assets/images/vila.jpg'),
                  ),
                  Container(
                    margin: EdgeInsets.only(
                      left: 15,
                    ),
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          'Mr. Vila Leng',
                          style: TextStyle(
                              fontSize: 20,
                              fontWeight: FontWeight.bold,
                              color: Colors.black54),
                        ),
                        Text(
                          'Software Developer',
                          style: TextStyle(
                            fontSize: 15,
                            fontWeight: FontWeight.bold,
                            color: Colors.black45,
                          ),
                        ),
                      ],
                    ),
                  ),
                  Icon(
                    Icons.flutter_dash,
                    size: 63,
                    color: Colors.blueGrey,
                  ),
                ],
              ),
              Padding(
                padding: const EdgeInsets.all(20),
                child: Text(
                  'My name is Mr. Vila Leng, you can call me Win, my ID is 6250110030. I am a junior student at Prince of Songkla University'
                  ', Trang Campus. My Major is Information and Computer Management(ICM).'
                  'I will be a skillful Software Developer.',
                  style: TextStyle(
                      fontSize: 15,
                      fontWeight: FontWeight.bold,
                      color: Colors.blueGrey),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(10),
                child: Text(
                  'The Best Automotive Showroom Ever',
                  style: TextStyle(
                      fontSize: 19,
                      fontWeight: FontWeight.bold,
                      color: Colors.black54),
                ),
              ),
              Divider(
                thickness: 0.8,
                color: Colors.blueGrey,
              ),
              Row(
                children: [
                  // CircleAvatar(
                  //   radius:168,
                  //   backgroundImage: AssetImage('assets/images/japan.jpg'),
                  // ),
                  Image.asset(
                    'assets/images/showroom2.jpg',
                    width: 369,
                    scale: 0.6,
                  ),
                ],
              ),
              Divider(
                thickness: 0.8,
                color: Colors.blueGrey,
              ),

              ElevatedButton.icon(
                onPressed: () {
                  //แทรกโค้ดให้คลิกแล้วกลับไปหน้าแรก
                  //
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => MyApp()));
                },
                icon: Icon(
                  Icons.arrow_back_ios,
                  color: Colors.white54,
                ),
                label: Text("Go Back",style: TextStyle(color: Colors.white54, fontWeight: FontWeight.bold, fontSize: 19),),
                style: ElevatedButton.styleFrom(primary: Colors.blueGrey),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
