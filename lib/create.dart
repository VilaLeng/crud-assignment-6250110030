import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:flutter_image_slideshow/flutter_image_slideshow.dart';
class Create extends StatefulWidget {
  @override
  _CreateState createState() => _CreateState();
}

class _CreateState extends State<Create> {
  // Handles text
  TextEditingController nameController = TextEditingController();
  TextEditingController priceController = TextEditingController();
  TextEditingController descriptionController = TextEditingController();

  // Http post request to create new data
  Future _createProducts() async {
    return await http.post(
      Uri.parse("http://192.168.2.120/android/insert_product.php"),
      body: {
        "name": nameController.text,
        "price": priceController.text,
        "description": descriptionController.text,
      },
    );
  }

  void _onConfirm(context) async {
    await _createProducts();

    // Remove all existing routes until the Home.dart, then rebuild Home.
    Navigator.of(context)
        .pushNamedAndRemoveUntil('/', (Route<dynamic> route) => false);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "\$ Add Products Page \$ ",
          style: TextStyle(
              color: Colors.white54, fontWeight: FontWeight.bold, fontSize: 25),
        ),
      ),
      bottomNavigationBar: BottomAppBar(
        child: ElevatedButton.icon(
          label: Text(
            'Save',
            style: TextStyle(
                color: Colors.white54,
                fontWeight: FontWeight.bold,
                fontSize: 19),
          ),
          icon: Icon(
            Icons.save,
            color: Colors.white54,
          ),
          style: ElevatedButton.styleFrom(
            primary: Colors.blue,
            onPrimary: Colors.white,
          ),
          onPressed: () {
            _onConfirm(context);
          },
        ),
      ),
      body: Container(
        height: double.infinity,
        padding: EdgeInsets.all(20),
        child: Column(
          children: <Widget>[
            Container(
                child: TextField(
              style: TextStyle(
                  color: Colors.blueGrey,
                  fontWeight: FontWeight.bold,
                  fontSize: 19),
              controller: nameController,
              decoration: InputDecoration(
                labelText: "Product Name:",
                hintText: "Enter Product Name",
              ),
            )),
            Container(
                child: TextField(
              style: TextStyle(
                  color: Colors.blueGrey,
                  fontWeight: FontWeight.bold,
                  fontSize: 19),
              controller: priceController,
              decoration: InputDecoration(
                labelText: "Price:",
                hintText: "Enter Product Price",
              ),
            )),
            Container(
                child: TextField(
              style: TextStyle(
                  color: Colors.blueGrey,
                  fontWeight: FontWeight.bold,
                  fontSize: 19),
              controller: descriptionController,
              decoration: InputDecoration(
                labelText: "Description:",
                hintText: "Enter Product Description",
              ),
            )),
            Padding(
              padding: EdgeInsets.all(5),
            ),
            /*Row(
              children: [
                // CircleAvatar(
                //   radius:168,
                //   backgroundImage: AssetImage('assets/images/japan.jpg'),
                // ),
                Image.asset(
                  'assets/images/showroom1.jpg',
                  width: 369,
                  scale: 0.6,
                ),
              ],
            ),
            Divider(
              thickness: 0.8,
              color: Colors.blueGrey,
            ),*/
           /* Divider(
              thickness: 0.8,
              color: Colors.blueGrey,
            ),*/

            ImageSlideshow(
              width: double.infinity,

              /// Height of the [ImageSlideshow].
              height: 200,

              /// The page to show when first creating the [ImageSlideshow].
              initialPage: 0,

              /// The color to paint the indicator.
              indicatorColor: Colors.blue,

              /// The color to paint behind th indicator.
              indicatorBackgroundColor: Colors.grey,

              /// The widgets to display in the [ImageSlideshow].
              /// Add the sample image file into the images folder
              children: [
                Image.asset(
                  'assets/images/car1.jpg',
                  fit: BoxFit.cover,
                ),
                Image.asset(
                  'assets/images/car2.jpg',
                  fit: BoxFit.cover,
                ),
                Image.asset(
                  'assets/images/car3.jpg',
                  fit: BoxFit.cover,
                ),
                Image.asset(
                  'assets/images/car4.jpg',
                  fit: BoxFit.cover,
                ),
                Image.asset(
                  'assets/images/car5.jpg',
                  fit: BoxFit.cover,
                ),
                Image.asset(
                  'assets/images/car6.jpg',
                  fit: BoxFit.cover,
                ),
              ],

              /// Called whenever the page in the center of the viewport changes.
              onPageChanged: (value) {
                print('Page changed: $value');
              },

              /// Auto scroll interval.
              /// Do not auto scroll with null or 0.
              autoPlayInterval: 3000,

              /// Loops back to first slide.
              isLoop: true,
            ),
            /* Row(
              children: [
                // CircleAvatar(
                //   radius:168,
                //   backgroundImage: AssetImage('assets/images/japan.jpg'),
                // ),
                Image.asset(
                  'assets/images/showroom2.jpg',
                  width: 339,
                  scale: 0.6,
                ),
              ],
            ),*/
            Divider(
              thickness: 0.8,
              color: Colors.blueGrey,
            ),
          /*  Divider(
              thickness: 0.8,
              color: Colors.blueGrey,
            ),*/
            Padding(
              padding: const EdgeInsets.all(10),
              child: Text(
                'Please, Enjoy Your Best Dream Luxury Cars of 2021, From the Slideshow Lists...',
                style: TextStyle(
                    fontSize: 15,
                    fontWeight: FontWeight.bold,
                    color: Colors.blueGrey),
              ),
            ),
            Divider(
              thickness: 0.8,
              color: Colors.blueGrey,
            ),

          ],
        ),
      ),
    );
  }
}
