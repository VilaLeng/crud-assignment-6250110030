import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

Widget createDrawerBodyItem(
    {required IconData icon, required String text, required GestureTapCallback onTap}) {
  return ListTile(
    title: Row(
      children: <Widget>[
        Icon(icon, size: 29,color: Colors.blueGrey,),
        Padding(
          padding: EdgeInsets.only(left: 8.0),
          child: Text(text, style: TextStyle(color: Colors.blueGrey, fontSize: 19, fontWeight: FontWeight.bold),),
        )
      ],
    ),
    onTap: onTap,
  );
}
