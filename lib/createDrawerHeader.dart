import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

Widget createDrawerHeader() {
  return DrawerHeader(
      margin: EdgeInsets.zero,
      padding: EdgeInsets.zero,
      decoration: BoxDecoration(
          image: DecorationImage(
              fit: BoxFit.fill, image: AssetImage('assets/images/showroom3.jpg')
          )
      ),
      child: Stack(children: <Widget>[

        Positioned(
            bottom: 12.0,
            left: 16.0,
            child: Text("Win World Automotive",
                style: TextStyle(
                    color: Colors.blueGrey,
                    // backgroundColor: Colors.white38,
                    backgroundColor: Colors.blueGrey[200],
                    fontSize: 27,
                    fontWeight: FontWeight.bold,
                    fontFamily: 'RobotoMono'
                )

            )
        ),
      ]));
}
