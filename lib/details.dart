import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'main.dart';
import 'products.dart';
import 'edit.dart';
import 'package:flutter_image_slideshow/flutter_image_slideshow.dart';
class Details extends StatefulWidget {
  final Products products;

  Details({required this.products});

  @override
  _DetailsState createState() => _DetailsState();
}

class _DetailsState extends State<Details> {
  void deleteProducts(context) async {
    await http.post(
      Uri.parse("http://192.168.2.120/android/delete_product.php"),
      body: {
        'pid': widget.products.pid.toString(),
      },
    );
    // Navigator.pop(context);
    Navigator.of(context)
        .pushNamedAndRemoveUntil('/', (Route<dynamic> route) => false);
  }

  void confirmDelete(context) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          content: Text(
            'Are you sure you want to delete this?',
            style: TextStyle(
                color: Colors.blueGrey,
                fontWeight: FontWeight.bold,
                fontSize: 19),
          ),
          actions: <Widget>[
            ElevatedButton.icon(
              label: Text(
                'No',
                style: TextStyle(
                    color: Colors.red,
                    fontWeight: FontWeight.bold,
                    fontSize: 25),
              ),
              icon: Icon(Icons.cancel),
              style: ElevatedButton.styleFrom(
                primary: Colors.grey,
                onPrimary: Colors.white,
              ),
              onPressed: () => Navigator.of(context).pop(),
            ),
            ElevatedButton.icon(
              label: Text(
                'Yes',
                style: TextStyle(
                    color: Colors.blueGrey,
                    fontWeight: FontWeight.bold,
                    fontSize: 25),
              ),
              icon: Icon(Icons.check_circle),
              style: ElevatedButton.styleFrom(
                primary: Colors.blue,
                onPrimary: Colors.white,
              ),
              onPressed: () => deleteProducts(context),
            ),
          ],
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          '\$ Products Details \$ ',
          style: TextStyle(
              color: Colors.white54, fontWeight: FontWeight.bold, fontSize: 25),
        ),
        actions: <Widget>[
          IconButton(
            icon: Icon(
              Icons.delete_sweep_outlined,
              color: Colors.white54,size: 33,
            ),
            onPressed: () => confirmDelete(context),
          ),
        ],
      ),
      body: Container(
        //height: 1537.0,
        padding: const EdgeInsets.all(35),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(
              "NAME : ${widget.products.name}",
              style: TextStyle(
                  color: Colors.blueGrey,
                  fontWeight: FontWeight.bold,
                  fontSize: 20),
            ),
            Padding(
              padding: EdgeInsets.all(10),
            ),
            Text(
              "PRICE : ${widget.products.price}",
              style: TextStyle(
                  color: Colors.blueGrey,
                  fontWeight: FontWeight.bold,
                  fontSize: 20),
            ),
            Padding(
              padding: EdgeInsets.all(10),
            ),
            Text(
              "DESCRIPTION : ${widget.products.description}",
              style: TextStyle(
                  color: Colors.blueGrey,
                  fontWeight: FontWeight.bold,
                  fontSize: 20),
            ),
            Padding(
              padding: EdgeInsets.all(10),
            ),
            Divider(
              thickness: 0.8,
              color: Colors.blueGrey,
            ),
            Padding(
              padding: const EdgeInsets.all(10),
              child: Text(
                'The Best Automotive Slideshow Ever',
                style: TextStyle(
                    fontSize: 19,
                    fontWeight: FontWeight.bold,
                    color: Colors.black54),
              ),
            ),
            Divider(
              thickness: 0.8,
              color: Colors.blueGrey,
            ),

            ImageSlideshow(
              width: double.infinity,

              /// Height of the [ImageSlideshow].
              height: 200,

              /// The page to show when first creating the [ImageSlideshow].
              initialPage: 0,

              /// The color to paint the indicator.
              indicatorColor: Colors.blue,

              /// The color to paint behind th indicator.
              indicatorBackgroundColor: Colors.grey,

              /// The widgets to display in the [ImageSlideshow].
              /// Add the sample image file into the images folder
              children: [
                Image.asset(
                  'assets/images/car1.jpg',
                  fit: BoxFit.cover,
                ),
                Image.asset(
                  'assets/images/car2.jpg',
                  fit: BoxFit.cover,
                ),
                Image.asset(
                  'assets/images/car3.jpg',
                  fit: BoxFit.cover,
                ),
                Image.asset(
                  'assets/images/car4.jpg',
                  fit: BoxFit.cover,
                ),
                Image.asset(
                  'assets/images/car5.jpg',
                  fit: BoxFit.cover,
                ),
                Image.asset(
                  'assets/images/car6.jpg',
                  fit: BoxFit.cover,
                ),
              ],

              /// Called whenever the page in the center of the viewport changes.
              onPageChanged: (value) {
                print('Page changed: $value');
              },

              /// Auto scroll interval.
              /// Do not auto scroll with null or 0.
              autoPlayInterval: 3000,

              /// Loops back to first slide.
              isLoop: true,
            ),
           /* Row(
              children: [
                // CircleAvatar(
                //   radius:168,
                //   backgroundImage: AssetImage('assets/images/japan.jpg'),
                // ),
                Image.asset(
                  'assets/images/showroom2.jpg',
                  width: 339,
                  scale: 0.6,
                ),
              ],
            ),*/
            Divider(
              thickness: 0.8,
              color: Colors.blueGrey,
            ),
            Padding(
              padding: const EdgeInsets.all(10),
              child: Text(
                'Please, Enjoy Your Best Dream Luxury Cars of 2021, From the Slideshow Lists...',
                style: TextStyle(
                    fontSize: 15,
                    fontWeight: FontWeight.bold,
                    color: Colors.blueGrey),
              ),
            ),
            Divider(
              thickness: 0.8,
              color: Colors.blueGrey,
            ),
            ElevatedButton.icon(
              onPressed: () {
                Navigator.push(
                    context, MaterialPageRoute(builder: (context) => Edit(products: widget.products)));
              },
              icon: Icon(
                Icons.border_color,
                color: Colors.white54,
              ),
              label: Text(
                "Edit Product",
                style: TextStyle(
                    color: Colors.white54,
                    fontWeight: FontWeight.bold,
                    fontSize: 19),
              ),
              style: ElevatedButton.styleFrom(primary: Colors.blueGrey),
            ),
            // Image.asset('assets/images/showroom1.jpg'),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(
          Icons.edit,
          color: Colors.white54,
          size: 31,
        ),
        onPressed: () => Navigator.of(context).push(
          MaterialPageRoute(
            builder: (context) => Edit(products: widget.products),
          ),
        ),
      ),
    );
  }
}
