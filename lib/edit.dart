import 'dart:io';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'products.dart';

class Edit extends StatefulWidget {
  final Products products;

  Edit({required this.products});

  @override
  _EditState createState() => _EditState();
}

class _EditState extends State<Edit> {
  // This is for text to edit
  TextEditingController pidController = TextEditingController();
  TextEditingController nameController = TextEditingController();
  TextEditingController priceController = TextEditingController();
  TextEditingController descriptionController = TextEditingController();

  // Http post request
  Future editProducts() async {
    return await http.post(
      Uri.parse("http://192.168.2.120/android/update_product.php"),
      body: {
        "pid": widget.products.pid.toString(),
        "name": nameController.text,
        "price": priceController.text,
        "description": descriptionController.text,
      },
    );
  }

  void _onConfirm(context) async {
    await editProducts();
    // Remove all existing routes until the Home.dart, then rebuild Home.
    Navigator.of(context)
        .pushNamedAndRemoveUntil('/', (Route<dynamic> route) => false);
  }

  @override
  void initState() {
    pidController = TextEditingController(text: widget.products.pid.toString());
    nameController = TextEditingController(text: widget.products.name);
    priceController = TextEditingController(text: widget.products.price);
    descriptionController =
        TextEditingController(text: widget.products.description);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "\$ Edit Products Page \$ ",
          style: TextStyle(
              color: Colors.white54, fontWeight: FontWeight.bold, fontSize: 25),
        ),
      ),
      bottomNavigationBar: BottomAppBar(
        child: ElevatedButton.icon(
          label: Text(
            'Save',
            style: TextStyle(
                color: Colors.white54,
                fontWeight: FontWeight.bold,
                fontSize: 19),
          ),
          icon: Icon(
            Icons.save,
            color: Colors.white54,
          ),
          style: ElevatedButton.styleFrom(
            primary: Colors.blue,
            onPrimary: Colors.white,
          ),
          onPressed: () {
            _onConfirm(context);
          },
        ),
      ),
      body: Container(
        height: double.infinity,
        padding: EdgeInsets.all(20),
        child: Column(
          children: <Widget>[
            Container(
                child: TextField(
              style: TextStyle(
                  color: Colors.blueGrey,
                  fontWeight: FontWeight.bold,
                  fontSize: 19),
              controller: pidController,
              decoration: InputDecoration(
                labelText: "Product ID:",
                hintText: "Enter Product ID",
              ),
            )),
            Container(
                child: TextField(
              style: TextStyle(
                  color: Colors.blueGrey,
                  fontWeight: FontWeight.bold,
                  fontSize: 19),
              controller: nameController,
              decoration: InputDecoration(
                labelText: "Product Name:",
                hintText: "Enter Product Name",
              ),
            )),
            Container(
                child: TextField(
              style: TextStyle(
                  color: Colors.blueGrey,
                  fontWeight: FontWeight.bold,
                  fontSize: 19),
              controller: priceController,
              decoration: InputDecoration(
                labelText: "Price:",
                hintText: "Enter Product Price",
              ),
            )),
            Container(
                child: TextField(
              style: TextStyle(
                  color: Colors.blueGrey,
                  fontWeight: FontWeight.bold,
                  fontSize: 19),
              controller: descriptionController,
              decoration: InputDecoration(
                labelText: "Description:",
                hintText: "Enter Product Description",
              ),
            )),
            Padding(
              padding: EdgeInsets.all(4.0),
            ),
            Row(
              children: [
                // CircleAvatar(
                //   radius:168,
                //   backgroundImage: AssetImage('assets/images/japan.jpg'),
                // ),
                Image.asset(
                  'assets/images/showroom1.jpg',
                  width: 369,
                  scale: 0.6,
                ),
              ],
            ),
            Divider(
              thickness: 0.8,
              color: Colors.blueGrey,
            ),
          ],
        ),
      ),
    );
  }
}
