import 'package:crudmysql_api/aboutPage.dart';
import 'package:crudmysql_api/theProductList.dart';
import 'package:crudmysql_api/products.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

import 'createDrawerBodyItem.dart';
import 'createDrawerHeader.dart';
import 'products.dart';
import 'details.dart';
import 'create.dart';

import 'package:flutter_image_slideshow/flutter_image_slideshow.dart';

import 'package:crudmysql_api/pageRoute.dart';
import 'package:flutter/cupertino.dart';

void main() {
  //runApp(const MyApp());
  runApp( MyApp());
}

class MyApp extends StatelessWidget {


  /*final images = [
    AssetImage("assets/images/vila.jpg"),
    AssetImage("assets/images/showroom1.jpg"),
    AssetImage("assets/images/showroom2.jpg"),
  ];*/


  // const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Home(),
      debugShowCheckedModeBanner: false,
      routes: {
        pageRoutes.about: (context) => aboutPage(),
        pageRoutes.list: (context) => listPage(),
      },

    );
  }
}

class Home extends StatefulWidget {
  @override
  HomeState createState() => HomeState();
}

class HomeState extends State<Home> {
  late Future<List<Products>> products;

  @override
  void initState() {
    super.initState();
    products = getProductsList();
  }

  Future<List<Products>> getProductsList() async {
    String url = "http://192.168.2.120/android/list_products.php";
    final response = await http.get(Uri.parse(url));
    if (response.statusCode == 200) {
      // If the server did return a 200 OK response,
      // then parse the JSON.
      return productsFromJson(response.body);
    } else {
      // If the server did not return a 200 OK response,
      // then throw an exception.
      throw Exception('Failed to load products');
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
         // 'Our Products List \$\$\$',
          'Best Luxury Cars For 2021',
          style: TextStyle(
              color: Colors.white54,
              fontWeight: FontWeight.bold,
              fontSize: 25),
        ),
      ),
      drawer: Drawer(
        backgroundColor: Colors.blueGrey[200],
        child: ListView(
          padding: EdgeInsets.zero,
          children: <Widget>[
            createDrawerHeader(),
            createDrawerBodyItem(
              icon: Icons.person,
              text: 'About Developer Page',
              onTap: () =>
              // Navigator.pushReplacementNamed(context, pageRoutes.home),
              Navigator.push(context,
                  MaterialPageRoute(
                      builder: (context) => aboutPage())),
            ),
            createDrawerBodyItem(
              icon: Icons.auto_stories_rounded,
              text: 'The Products List Page',
              onTap: () =>
              // Navigator.pushReplacementNamed(context, pageRoutes.home),
              Navigator.push(context,
                  MaterialPageRoute(
                      builder: (context) => MyApp())),
            ),
            createDrawerBodyItem(
              icon: Icons.book,
              text: 'The Products News Page',
              onTap: () =>
              // Navigator.pushReplacementNamed(context, pageRoutes.home),
              Navigator.push(context,
                  MaterialPageRoute(
                      builder: (context) => listPage())),
            ),

            // createDrawerHeader(),
          ],
        ),

      ),
      body: Center(
        child: FutureBuilder<List<Products>>(
          future: products,
          builder: (BuildContext context, AsyncSnapshot snapshot) {
            // By default, show a loading spinner.
            if (!snapshot.hasData) return CircularProgressIndicator();
            // Render student lists
            return ListView.builder(
              itemCount: snapshot.data.length,
              itemBuilder: (BuildContext context, int index) {
                var data = snapshot.data[index];
                return Card(
                  child: ListTile(
                    leading: Icon(Icons.directions_car, color: Colors.blueGrey,),
                    /*leading: CircleAvatar(
                      backgroundImage: images[index],
                      radius: 30,
                    ),*/
                    trailing: Icon(Icons.collections_bookmark_outlined, color: Colors.blueGrey,),
                    title: Text(
                      data.name,
                      style: TextStyle(color: Colors.blueGrey,
                          fontWeight: FontWeight.bold,fontSize: 20),
                    ),
                    onTap: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => Details(products: data)),
                      );
                    },
                  ),

                );
              },
            );
          },
        ),

      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add_to_photos_outlined, color: Colors.white54, size: 35,),
        onPressed: () {
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => Create()),
          );
        },
      ),
    );
  }
}
