import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:crudmysql_api/pageRoute.dart';
import 'package:crudmysql_api/createDrawerBodyItem.dart';
import 'package:crudmysql_api/createDrawerHeader.dart';

class navigationDrawer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var pageRoutes;
    return Drawer(
      backgroundColor: Colors.blueGrey[200],
      child: ListView(
        padding: EdgeInsets.zero,
        children: <Widget>[
          createDrawerHeader(),
          createDrawerBodyItem(
            icon: Icons.person,
            text: 'About Page',
            onTap: () =>
                Navigator.pushReplacementNamed(context, pageRoutes.about),
          ),
          // createDrawerHeader(),
        ],
      ),
    );
  }
}
