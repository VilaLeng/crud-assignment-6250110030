import 'package:crudmysql_api/products.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:crudmysql_api/theProductList.dart';
import 'package:crudmysql_api/navigationDrawer.dart';
import 'package:crudmysql_api/createDrawerBodyItem.dart';
import 'package:crudmysql_api/createDrawerHeader.dart';
import 'package:http/http.dart' as http;
import '../main.dart';
import 'details.dart';
import 'theProductList.dart';
import 'aboutPage.dart';
import 'package:url_launcher/url_launcher.dart';

class listPage extends StatelessWidget {
  /*late Future<List<Products>> products;

  @override
  void initState() {
    super.initState();
    products = getProductsList();
  }

  Future<List<Products>> getProductsList() async {
    String url = "http://192.168.2.120/android/list_products.php";
    final response = await http.get(Uri.parse(url));
    if (response.statusCode == 200) {
      // If the server did return a 200 OK response,
      // then parse the JSON.
      return productsFromJson(response.body);
    } else {
      // If the server did not return a 200 OK response,
      // then throw an exception.
      throw Exception('Failed to load products');
    }
  }*/

  static const String routeName = '/TheProductList';

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        appBar: AppBar(
          title: Text(
            "The Products News Page",
            style: TextStyle(
                color: Colors.white54,
                fontSize: 25,
                fontWeight: FontWeight.bold),
          ),
        ),
        drawer: Drawer(
          backgroundColor: Colors.blueGrey[200],
          child: ListView(
            padding: EdgeInsets.zero,
            children: <Widget>[
              createDrawerHeader(),
              createDrawerBodyItem(
                icon: Icons.person,
                text: 'About Developer Page',
                onTap: () =>
                    // Navigator.pushReplacementNamed(context, pageRoutes.home),
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) => aboutPage())),
              ),
              createDrawerBodyItem(
                icon: Icons.auto_stories_rounded,
                text: 'The Products List Page',
                onTap: () =>
                    // Navigator.pushReplacementNamed(context, pageRoutes.home),
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) => MyApp())),
              ),
              // createDrawerHeader(),
              createDrawerBodyItem(
                icon: Icons.book,
                text: 'The Products News Page',
                onTap: () =>
                    // Navigator.pushReplacementNamed(context, pageRoutes.home),
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) => listPage())),
              ),
            ],
          ),
        ),
        body: GridView.count(
          primary: false,
          padding: const EdgeInsets.all(20),
          crossAxisSpacing: 15,
          mainAxisSpacing: 15,
          crossAxisCount: 2,
          children: <Widget>[
            /*Container(
              padding: const EdgeInsets.all(8),
              child: const Text(
                "Start-Up Investment Plan",
                style: TextStyle(
                    color: Colors.white,
                    fontSize: 21,
                    fontWeight: FontWeight.bold),
              ),
              color: Colors.blueGrey[600],
            ),*/
            /*ElevatedButton.icon(
              onPressed: () {
                //แทรกโค้ดให้คลิกแล้วกลับไปหน้าแรก
                //
                Navigator.push(context,
                    MaterialPageRoute(
                        builder: (context) => MyApp()));


              },
              icon: Icon(
                Icons.arrow_back_ios,
                color: Colors.blueGrey,
              ),
              label: Text("Go Back",style: TextStyle(
                  color: Colors.blueGrey,
                  fontSize: 21,
                  fontWeight: FontWeight.bold),),
              style: ElevatedButton.styleFrom(primary: Colors.blueGrey[100]),
            ),*/

            ElevatedButton(
              onPressed: () {
                //แทรกโค้ดให้คลิกแล้วกลับไปหน้าแรก
                //
                Navigator.push(
                    context, MaterialPageRoute(builder: (context) => MyApp()));
              },
              style: ElevatedButton.styleFrom(
                primary: Colors.blueGrey[600],
              ),
              child: Text(
                '2021 Audi A8 News',
                style: TextStyle(
                    color: Colors.white,
                    fontSize: 21,
                    fontWeight: FontWeight.bold),
              ),
             /* onPressed: () async {
                String url = "https://www.forbes.com/wheels/best/luxury-cars/";
                var urllaunchable = await canLaunch(url); //canLaunch is from url_launcher package
                if(urllaunchable){
                  await launch(url); //launch is from url_launcher package to launch URL
                }else{
                  print("URL can't be launched.");
                }
              },*/
            ),
           /* Container(
              padding: const EdgeInsets.all(8),
              child: const Text(
                'Stock Investment Plan',
                style: TextStyle(
                    color: Colors.white,
                    fontSize: 21,
                    fontWeight: FontWeight.bold),
              ),
              color: Colors.blueGrey[200],
            ),*/
            ElevatedButton(
              onPressed: () {
                //แทรกโค้ดให้คลิกแล้วกลับไปหน้าแรก
                //
                Navigator.push(
                    context, MaterialPageRoute(builder: (context) => MyApp()));
              },
              style: ElevatedButton.styleFrom(
                primary: Colors.blueGrey[200],
              ),
              child: Text(
                '2021 BMW 5 Series News',
                style: TextStyle(
                    color: Colors.white,
                    fontSize: 21,
                    fontWeight: FontWeight.bold),
              ),
            ),
            /*Container(
              padding: const EdgeInsets.all(8),
              child: const Text(
                'Realestate Investment Plan ',
                style: TextStyle(
                    color: Colors.white,
                    fontSize: 21,
                    fontWeight: FontWeight.bold),
              ),
              color: Colors.blueGrey[300],
            ),*/
            ElevatedButton(
              onPressed: () {
                //แทรกโค้ดให้คลิกแล้วกลับไปหน้าแรก
                //
                Navigator.push(
                    context, MaterialPageRoute(builder: (context) => MyApp()));
              },
              style: ElevatedButton.styleFrom(
                primary: Colors.blueGrey[300],
              ),
              child: Text(
                '2021 Lexus LS News',
                style: TextStyle(
                    color: Colors.white,
                    fontSize: 21,
                    fontWeight: FontWeight.bold),
              ),
            ),
            /*Container(
              padding: const EdgeInsets.all(8),
              child: const Text(
                'Block Chain Invesment Plan',
                style: TextStyle(
                    color: Colors.white,
                    fontSize: 21,
                    fontWeight: FontWeight.bold),
              ),
              color: Colors.blueGrey[400],
            ),*/
            ElevatedButton(
              onPressed: () {
                //แทรกโค้ดให้คลิกแล้วกลับไปหน้าแรก
                //
                Navigator.push(
                    context, MaterialPageRoute(builder: (context) => MyApp()));
              },
              style: ElevatedButton.styleFrom(
                primary: Colors.blueGrey[400],
              ),
              child: Text(
                '2021 Genesis G80 News',
                style: TextStyle(
                    color: Colors.white,
                    fontSize: 21,
                    fontWeight: FontWeight.bold),
              ),
            ),
           /* Container(
              padding: const EdgeInsets.all(8),
              child: const Text(
                'Cryptocurrency Investment Plan',
                style: TextStyle(
                    color: Colors.white,
                    fontSize: 21,
                    fontWeight: FontWeight.bold),
              ),
              color: Colors.blueGrey[500],
            ),*/
            ElevatedButton.icon(
              onPressed: () {
                //แทรกโค้ดให้คลิกแล้วกลับไปหน้าแรก
                //
                Navigator.push(
                    context, MaterialPageRoute(builder: (context) => MyApp()));
              },
              icon: Icon(
                Icons.arrow_back_ios,
                color: Colors.blueGrey,
              ),
              label: Text(
                "Go Back",
                style: TextStyle(
                    color: Colors.blueGrey,
                    fontSize: 21,
                    fontWeight: FontWeight.bold),
              ),
              style: ElevatedButton.styleFrom(primary: Colors.blueGrey[100]),
            ),
            ElevatedButton(
              onPressed: () {
                //แทรกโค้ดให้คลิกแล้วกลับไปหน้าแรก
                //
                Navigator.push(
                    context, MaterialPageRoute(builder: (context) => MyApp()));
              },
              style: ElevatedButton.styleFrom(
                primary: Colors.blueGrey[500],
              ),
              child: Text(
                'Start-Up Investment Plan',
                style: TextStyle(
                    color: Colors.white,
                    fontSize: 21,
                    fontWeight: FontWeight.bold),
              ),
            ),
            /*Container(
              padding: const EdgeInsets.all(8),
              child: const Text('NFT Investment Plan',style: TextStyle(
                  color: Colors.white,
                  fontSize: 21,
                  fontWeight: FontWeight.bold),),
              color: Colors.blueGrey[600],
            ),*/

          ],
        ));
  }
}
